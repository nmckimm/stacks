package answers;
import java.util.EmptyStackException;
import java.util.Stack;

public class ArrayStack<E>{
    private E[] S;
    protected int capacity;
    public static final int CAPACITY = 1000;
    protected int top = -1;

    public ArrayStack() {
        this.capacity = CAPACITY;
        S = (E[]) new Object[capacity];
    }

    public ArrayStack(int cap) {
        this.capacity = cap;
        S = (E[]) new Object[capacity];
    }

    public static void main(String[] args) {

//        Object o;
//        ArrayStack<Integer> A = new ArrayStack<Integer>();
//        A.status("new ArrayStack<Integer> A", null);
//        A.push(7);
//        A.status("A.push(7)", null);
//        o = A.pop();
//        A.status("A.pop()", o);
//        A.push(9);
//        A.status("A.push(9)", null);
//        o = A.pop();
//        A.status("A.pop()", o);
//        ArrayStack<String> B = new ArrayStack<String>();
//        B.status("new ArrayStack<String> B", null);
//        B.push("Bob");
//        B.status("B.push(\"Bob\")", null);
//        B.push("Alice");
//        B.status("B.push(\"Alice\")", null);
//        o = B.pop();
//        B.status("B.pop()", o);
//        B.push("Eve");
//        B.status("B.push(\"Eve\")", null);

        ArrayStack s = new ArrayStack();
        System.out.println(s);
        s.push("A");
        System.out.println(s);
        s.push("B");
        System.out.println(s);
        s.push("C");
        System.out.println(s);
    }

    public String toString()
    {
        String s;
        s = "";
        if (size() > 0)
        {
            s += S[0];
        }
        if (size() > 1) {
            for (int i = 1; i <= size() - 1; i++)
            {
                s += " " + S[i];
            }
        }

        return "" + size() + "\t" + s;
    }


    public int size() {
        return top + 1;
    }


    public boolean isEmpty() {
        if (top < 0) {
            return true;
        } else {
            return false;
        }
    }

    public void status(String op, Object element)
    {
        System.out.print("------>" + op);
        System.out.println(", returns " + element);

        System.out.print("result: size = " + size() + ", isEmpty = " + isEmpty());
        System.out.println(", stack: " + this);

    }


    public void push(E element) throws FullStackException {
        if (size() == capacity) throw new FullStackException();
        else {
            S[++top] = element;
        }
    }

    public E top() throws EmptyStackException {
        if (isEmpty()) throw new EmptyStackException();
        else {
            return S[top];
        }
    }


    //****
    public E pop() throws EmptyStackException {
        E element;
        if (isEmpty()) throw new EmptyStackException();
        else {
            element = S[top];
            S[top--] = null;
            return element;

        }



    }


}
