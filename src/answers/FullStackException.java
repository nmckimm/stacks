package answers;

public class FullStackException extends RuntimeException {

    public FullStackException() {
        this("The stack is full");
    }

    public FullStackException(String message) {
        super(message);
    }

}