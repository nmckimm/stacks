package answers;

public class LinkedStack {
    public class Node {
        Object element;
        Node next;
        public Node(Object element) {
            this.element = element;
        }
        public String toString() {
            return element.toString();
        }
    }
    int size;
    Node top;
    public LinkedStack() {
        top = null;
        size = 0;
    }

    public static void main(String[] args) {

        LinkedStack s = new LinkedStack();
        System.out.println(s);
        s.push("A");
        System.out.println(s);
        s.push("B");
        System.out.println(s);
        s.push("C");
        System.out.println(s);
    }



    public void push(Object o) {
        Node node = new Node(o);
        node.next = top;
        top = node;
        size++;
    }
    public Object pop() {
        Object e = top.element;
        top.element = null;
        top = top.next;
        --size;
        return e;

    }
    public Object top() {
        Object e = top.element;
        return e;
    }

    public int size() {
        return this.size;
    }
    public boolean isEmpty() {
        if (size() < 0) { return true; }
        else { return false; }
    }
    // state visualisation
    public String toString() {
        String output = "";
        Node node = top;
        while (node != null) {
            output = node.element.toString() + " " + output;
            node = node.next;
        }
        return "" + size + "\t" + output;
    }
}