package answers;

public class PCQ2A {
    public static void main(String[] args)
    {
        ArrayStack s = new ArrayStack();

        /* I was unsure whether the note in QC was telling us to
        push like this. My compiler says this method has been deprecated
        so I have included both methods to be safe.
         */

        System.out.println(s);
        s.push(new Character('e'));
        System.out.println(s);
        s.push(new Character('s'));
        System.out.println(s);
        s.push(new Character('c'));
        System.out.println(s);
        s.pop();
        System.out.println(s);
        s.push(new Character('u'));
        System.out.println(s);
        s.push(new Character('a'));
        System.out.println(s);
        s.pop();
        System.out.println(s);
        s.push(new Character('o'));
        System.out.println(s);
        s.push(new Character('t'));
        System.out.println(s);
        s.pop();
        System.out.println(s);
        s.push(new Character('h'));
        System.out.println(s);

        /* Original:
        System.out.println(s);
        s.push('e');
        System.out.println(s);
        s.push('s');
        System.out.println(s);
        s.push('c');
        System.out.println(s);
        s.pop();
        System.out.println(s);
        s.push('u');
        System.out.println(s);
        s.push('a');
        System.out.println(s);
        s.pop();
        System.out.println(s);
        s.push('o');
        System.out.println(s);
        s.push('t');
        System.out.println(s);
        s.pop();
        System.out.println(s);
        s.push('h');
        System.out.println(s);
         */

    }
}
