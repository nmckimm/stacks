package answers;

public class PCQ3A {

   public static void main(String[] args)
   {
       ArrayStack s = new ArrayStack();

       System.out.println(s);
       s.push("Ireland");
       System.out.println(s);
       s.pop();
       System.out.println(s);
       s.push("England");
       System.out.println(s);
       s.pop();
       System.out.println(s);
       s.push("Wales");
       System.out.println(s);
       s.pop();
       System.out.println(s);
       s.push("Scotland");
       System.out.println(s);
       s.pop();
       System.out.println(s);
       s.push("France");
       System.out.println(s);
       s.push("Germany");
       System.out.println(s);

   }
}
